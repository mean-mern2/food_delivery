const User = require('../Model/index');

//function to place an order
module.exports.placeorder = async (req, res, next) => {
    try {
        const userId = req.params.userid;
        const itemId = req.body.itemId;
        const quantity = req.body.quantity || 1;

        //finding if userid is valid
        const myuser = await User.User.findById(userId);
        if (!myuser) {
            return res.status(404).json({
                message: "User not found"
            });
        }
         //finding if itemid is valid
        const myitem = await User.MenuItems.menuSchema.findOne({
            "_id": itemId,
            "isAvailable": true
        });
        if (!myitem) {
            return res.status(404).json({
                message: "item not found"
            });
        }

        const price = myitem.price;
        const totalBill = (price * quantity);


        let user = await User.Order.orderSchema.create({
            userId,
            itemId,
            quantity,
            totalBill
        });
        if (!user) {
            res.status(400).json({
                message: "unable to place order"
            });
        }

        return res.status(200).json({
            user
        });


    } catch (err) {
        next(err);
    }
};

//function to get all detials of a particular order
module.exports.orderDetails = async (req, res, next) => {
    try {
        const id = req.params.id;

        let users = await User.Order.orderSchema.findById(id).populate({
            path: 'itemId',
            populate: {
                path: 'storeId'
            }
        }).populate({
            path: 'userId'
        });

        if (!users || users.length == 0) {
            return res.status(400).json({
                message: "does not have any user"
            });
        }


        return res.status(200).json({
            users
        });



    } catch (err) {
        next(err);
    }
};