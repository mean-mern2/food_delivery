
const User = require('../Model/index');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const validations = require('../Validations/userValidations');

const secret_key = "abc@apptunix2121";

//function to get all users
module.exports.getallUser = async (req, res, next) => {
    try {

        const limit = Number(req.query.limit)|| 3;
        const page = Number(req.query.page)||1;
        let skip = (page-1)*limit;
        let str= req.query.str || "";
      

        str=str.trim();
        let counts;
        if(str)
        {
            let userss = await User.User.find({"isDeactivated":false,"firstname" : {$regex :  str}}).skip(skip).limit(limit);
            
            if (!userss || userss.length == 0) 
            {     return res.status(400).json({ message: "match not found " })  ;  }

            
            var totalpage = Math.ceil(counts/limit);
            return res.status(200).json({ "user":userss,"len":totalpage });
        }
        else{  
            let users = await User.User.find({"isDeactivated":false}).select("-password").skip(skip).limit(limit);
            if (!users || users.length == 0) 
            {     return res.status(400).json({ message: "does not have any user" }) ;   }
            
            counts= await User.User.countDocuments();
            totalpage = Math.ceil(counts/limit);
            return res.status(200).json({ "user":users,"len":totalpage });
        }
              
    }
    catch (err) {
        next(err);
    }
};


//function to register a user
module.exports.register = async (req, res) => {
    try {
        
        await validations.joiSchema.validateAsync(req.body);
        let { firstname, lastname, email, password,phoneNo,isDeactivated,profilePic } = req.body;
            
        //---- checking duplicacy in email and  number ----//
        const myuser2 = await User.User.findOne({  email });
        if (myuser2) { throw new Error("Sorry this email already exist"); }
        if(phoneNo){
            const myuser = await User.User.findOne({  phoneNo  });
            if (myuser ) { throw new Error("Sorry this number already exist"); }
        }
          
        
        let user = await User.User.create({ firstname,  lastname, email, password, phoneNo,isDeactivated,profilePic });
        if (!user) 
        {    res.status(400).json({ message: "unable to register student" });    }
        
        return res.status(201).json({ user });

    }
    catch (err) {
        console.log(err, "-----");
        return res.status(400).json({ message: err });
    }
};


//function to get a user
module.exports.getstudent = async (req, res, next) => {
    try {
        const id = req.params.id;
        const myuser = await User.User.findOne({"id":id,"isDeactivated":false}).select("-password");
        if (!myuser) 
        {    return res.status(404).json({ message: "user not found" })  ;  }

        return res.status(200).json({ myuser });


    }
    catch (err) {
        next(err);
    }
};


//function to login a user
module.exports.loginpage = async (req, res, next) => {
    try {
        const email = req.body.email;
        const password = req.body.password;
        let myuser;
        


        if (email) 
        {
            myuser = await User.User.findOne({ "email": email,"isDeactivated":false });
            if(!myuser)
            {
                throw new Error("User Not Found");
            }
        }
        else
        {   throw new Error("provide required credentials");  }
        
        let pas;
        if (password) 
        {  pas = await bcrypt.compare(password, myuser.password); }
        else 
        {  throw new Error("password not provided");  }

        if(!pas)
        {res.json("password incorrect"); }        
        

        const idd= myuser._id;

        const   jti=Date.now().toString();
        
        const authtoken = jwt.sign({idd,jti}, secret_key, { expiresIn: "2h" });
         await User.User.findByIdAndUpdate(idd,{$set:{"validtoken": jti}});
        

        return res.json({ authtoken });

        
    }
    catch (err) {
        console.log(err);
        next(err);
    }
};




//function to delete a user
module.exports.deleteuser = async (req, res, next) => {
    try {

        const id = req.params.id;
        let user = await User.User.findByIdAndDelete(id);
        if (!user) 
        {    return res.status(400).json({ message: "Unable to delete user" });   }


        return res.status(200).json({ message: "Successfully Deleted" });
    }
    catch (err) {
        next(err);
    }
};


//function to delete a user
module.exports.updateuser = async (req, res) => {

    try {
        await validations.joiSchema.validate(req.body);
        
        const myuser = await User.User.findById(req.params.id);
        if (!myuser) 
        {    return res.status(404).json({ message: "user not found" })  ;  }

        let data = await User.User.updateOne(
            {   "_id":req.params.id}, 
            {   $set: req.body}
        );

        if(!data)
        {   return res.status(400).json({message: "Unable to save user"});  }
        
        res.send(data);
    } 
    catch (error) 
    {    res.status(500).send("Internal Server Error");     }

};



//function to get all profile details of a user 
module.exports.getprofile = async (req, res, next) => {
    try {
        const token1 = req.header("auth-token");
        if (!token1) 
        {    res.status(401).send({ error: "please authenticate using a valid token" });    }

        
        const decoded =  await jwt.verify(token1, secret_key, (err, decoded1) => {
            return decoded1;
        }); 
        
        let myuser;
            myuser = await User.User.findById(decoded.idd).select("-password");
            if(myuser)
            {  
                if (decoded.jti != myuser.validtoken) 
                {
                    throw new Error("JWT ID is invalid");
                }
            }
            else
            {
                return res.status(400).json({message:"user not found"});
            }

        res.send(myuser);

    }
    catch (err) {
        next(err);
    }
};


//function to get all orders of a user
module.exports.findallorders = async (req, res, next) => {
    try {
        const id = req.params.id;
    
        const myuser = await User.Order.orderSchema.find({"userId":id});
        if (!myuser) 
        {    return res.status(404).json({ message: "order not found" })  ;  }
        console.log(myuser);
        return res.status(200).json({ myuser });


    }
    catch (err) {
        next(err);
    }
};














