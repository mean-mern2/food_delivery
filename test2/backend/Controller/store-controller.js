const User = require('../Model/index');
const validations = require('../Validations/storeValidations');

//function to get list of all stores
module.exports.getAllStores = async (req, res, next) => {
    try {

        const limit = Number(req.query.limit) || 3;
        const page = Number(req.query.page) || 1;
        let skip = (page - 1) * limit;
        let str = req.query.str || "";


        str = str.trim();
        let counts;
        if (str) {
            let userss = await User.Store.storeSchema.find({
                "storeName": {
                    $regex: str
                }
            }).skip(skip).limit(limit);

            if (!userss || userss.length == 0) {
                return res.status(400).json({
                    message: "match not found "
                });
            }


            var totalpage = Math.ceil(counts / limit);
            return res.status(200).json({
                "user": userss,
                "len": totalpage
            });
        } else {
            let users = await User.Store.storeSchema.find({}).skip(skip).limit(limit);
            if (!users || users.length == 0) {
                return res.status(400).json({
                    message: "does not have any Store"
                });
            }

            counts = await User.Store.storeSchema.countDocuments();
            totalpage = Math.ceil(counts / limit);
            return res.status(200).json({
                "user": users,
                "len": totalpage
            });
        }

    } catch (err) {
        next(err);
    }
};


//function to register a store
module.exports.registerStore = async (req, res) => {
    try {

        await validations.joiSchema.validateAsync(req.body);
        let {
            storeName,
            location
        } = req.body;

     //---- checking duplicacy in storeName and location ----//
        const myuser2 = await User.User.findOne({
            "storeName": storeName,
            "location": location
        });
        if (myuser2) {
            throw new Error("Sorry this restaurent already exist");
        }



        let user = await User.Store.storeSchema.create({
            storeName,
            location
        });
        if (!user) {
            res.status(400).json({
                message: "unable to register Store"
            });
        }

        return res.status(201).json({
            user
        });

    } catch (err) {
        console.log(err, "-----");
        return res.status(400).json({
            message: err
        });
    }
};


//function to get a particular store
module.exports.getStore = async (req, res, next) => {
    try {
        const id = req.params.id;

        const myuser = await User.Store.storeSchema.findOne({
            "_id": id
        });
        if (!myuser) {
            return res.status(404).json({
                message: "Store not found"
            });
        }

        return res.status(200).json({
            myuser
        });


    } catch (err) {
        next(err);
    }
};

//function to delete a particular store
module.exports.deleteStore = async (req, res, next) => {
    try {

        const id = req.params.id;
        let user = await User.Store.storeSchema.findByIdAndDelete(id);
        if (!user) {
            return res.status(400).json({
                message: "Unable to delete Store"
            });
        }
        
        //also delete items of store from items schema
        let user2 = await User.MenuItems.menuSchema.findOneAndDelete({
            "storeId": id
        });

        if (!user2) {
            return res.status(400).json({
                message: "Unable to delete respective Items"
            });
        }

        return res.status(200).json({
            message: "Successfully Deleted"
        });
    } catch (err) {
        next(err);
    }
};

//function to update a particular store
module.exports.updateStore = async (req, res) => {

    try {
        await validations.joiSchema.validate(req.body);

        const myuser = await User.Store.storeSchema.findById(req.params.id);
        if (!myuser) {
            return res.status(404).json({
                message: "Store not found"
            });
        }

        let data = await User.Store.storeSchema.updateOne({
            "_id": req.params.id
        }, {
            $set: req.body
        });

        if (!data) {
            return res.status(400).json({
                message: "Unable to save Store"
            });
        }

        res.send(data);
    } catch (error) {
        res.status(400).send("----ERROR----");
    }

};