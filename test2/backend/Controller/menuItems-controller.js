const User = require('../Model/index');
const validations = require('../Validations/menuItemsValidations');

//function to get all menu items
module.exports.getAllMenuItems = async (req, res, next) => {
    try {

        await validations.joiSchema.validateAsync(req.body);

        const limit = Number(req.query.limit) || 3;
        const page = Number(req.query.page) || 1;
        let skip = (page - 1) * limit;
        let str = req.query.str || "";

        str = str.trim();
        let counts;
        if (str) {    // block will be executed in case of search
            let userss = await User.MenuItems.menuSchema.find({
                "itemName": {
                    $regex: str
                }
            }).skip(skip).limit(limit);

            if (!userss || userss.length == 0) {
                return res.status(400).json({
                    message: "match not found "
                });
            }


            var totalpage = Math.ceil(counts / limit);
            return res.status(200).json({
                "item": userss,
                "len": totalpage
            });
        } else {   // block will be executed in normal case without search
            let users = await User.MenuItems.menuSchema.find({}).skip(skip).limit(limit);
            if (!users || users.length == 0) {
                return res.status(400).json({
                    message: "does not have any Item "
                });
            }

            counts = await User.MenuItems.menuSchema.countDocuments();
            totalpage = Math.ceil(counts / limit);
            return res.status(200).json({
                "item": users,
                "len": totalpage
            });
        }




    } catch (err) {
        next(err);
    }
};


//function to get add a menu item
module.exports.addMenuItem = async (req, res) => {
    try {

        await validations.joiSchema.validateAsync(req.body);
        let {
            storeId,
            itemName,
            price,
            isAvailable
        } = req.body;



        //---- checking duplicacy in email and  number ----//
        const myuser2 = await User.MenuItems.menuSchema.findOne({
            "itemName": itemName,
            "storeId": storeId
        });
        if (myuser2) {
            throw new Error("Sorry this item on this restaurent already exists");
        }

        //searching
        const store = await User.Store.storeSchema.findById(storeId);
        const storeName = store.storeName;
        let user = await User.MenuItems.menuSchema.create({
            storeId,
            storeName,
            itemName,
            price,
            isAvailable
        });
        if (!user) {
            res.status(400).json({
                message: "unable to add item"
            });
        }

        return res.status(201).json({
            user
        });

    } catch (err) {
        console.log(err, "-----");
        return res.status(400).json({
            message: err
        });
    }
};

//function to seacrh a menu item
module.exports.getAnItem = async (req, res, next) => {
    try {
        const id = req.params.id;

        const myuser = await User.MenuItems.menuSchema.findById(id);
        if (!myuser) {
            return res.status(404).json({
                message: "Item not found"
            });
        }

        return res.status(200).json({
            myuser
        });


    } catch (err) {
        next(err);
    }
};


//function to get all items of a store
module.exports.getStoreItems = async (req, res, next) => {
    try {
        const id = req.params.id;

        const myuser = await User.MenuItems.menuSchema.find({
            "storeId": id
        });
        if (!myuser) {
            return res.status(404).json({
                message: "Item not found"
            });
        }

        return res.status(200).json({
            myuser
        });


    } catch (err) {
        next(err);
    }
};

//function to delete an item
module.exports.deleteItem = async (req, res, next) => {
    try {

        const id = req.params.id;
        let user = await User.MenuItems.menuSchema.findByIdAndDelete(id);
        if (!user) {
            return res.status(400).json({
                message: "Unable to delete Item"
            });
        }

        return res.status(200).json({
            message: "Successfully Deleted"
        });
    } catch (err) {
        next(err);
    }
};

//function to update an item
module.exports.updateItem = async (req, res) => {

    try {
        await validations.joiSchema.validate(req.body);

        //to check if the item exist
        const myuser = await User.MenuItems.menuSchema.findById(req.params.id);
        if (!myuser) {
            return res.status(404).json({
                message: "Item not found"
            });
        }

        let data = await User.MenuItems.menuSchema.updateOne({
            "_id": req.params.id
        }, {
            $set: req.body
        });

        if (!data) {
            return res.status(400).json({
                message: "Unable to Update Item"
            });
        }

        res.send(data);
    } catch (error) {
        res.status(400).send("----ERROR----");
    }

};