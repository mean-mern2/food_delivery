module.exports = {
    User: require("./userModel"),
    Store: require("./storesModel"),
    MenuItems: require("./menuItemsSchema"),
    Order: require('./orderSchema'),
    UserRoom: require("./UserRoom"),
    ChatMessage: require("./ChatMessage")
};