const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");

const docSchema = new Schema({
  firstname: {
    type: String,
    required: true,
    minLength: 3
  },
  lastname: {
    type: String,
    required: true,
    minLength: 3
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minLength: 6
  },
  isDeactivated:{
    type:Boolean,
    required:true
  },
  phoneNo:{
    type: String
  },
  profilePic:{
    type:String
  },
  validtoken:{
    type:String
    
}
});

docSchema.pre("save",async function(next){
    try{
        const salt= 10;
        const hashh = await bcrypt.hash(this.password,salt);
        this.password=hashh;
        next();
    }
    catch(err)
    {
        next(err);
    }

});

const userSchema = mongoose.model("user", docSchema);
module.exports =  userSchema ;
