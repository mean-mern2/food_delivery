
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const docSchema = new Schema({
    storeId:{
        type:mongoose.Types.ObjectId,
        required : true,
        ref:"stores"
    },
    storeName:{
        type:String,
        required : true
        
    },
    itemName:{
        type:String,
        required : true
    },
    price:{
        type:Number,
        required : true
    },
    isAvailable:{
        type:Boolean,
        required : true
    }
    
});

const menuSchema= mongoose.model("menuItems",docSchema);
module.exports= {menuSchema};








