
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const docSchema = new Schema({
    userId:{
        type:mongoose.Types.ObjectId,
        ref:"user"
    },
    itemId:{
        type:mongoose.Types.ObjectId,
        ref:"menuItems"
    },
    quantity:{
        type:Number
    },
    totalBill:{
        type:Number
       
    }
    
});

const orderSchema= mongoose.model("orders",docSchema);
module.exports= {orderSchema};








