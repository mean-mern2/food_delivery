const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

const docSchema = new Schema({
  Room_id: {
    type: ObjectId,
    required: true,
    ref:"room"
  },
  Messages: {
    type: String,
    required: true
  },
  senderId:{
    type: ObjectId,
    required:true,
    ref:"user"
  },
  receiverId:{
    type: ObjectId,
    required:true,
    ref:"stores"
  }
});

const UserSchema = mongoose.model("message", docSchema);
module.exports = UserSchema;
