const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

const docSchema = new Schema({
  User1: {
    type: ObjectId,
    ref: "user",
    required: true
  },
  User2: {
    type: ObjectId,
    ref: "user",
    required: true
  }
  
});

const UserSchema = mongoose.model("room", docSchema);
module.exports = { UserSchema };
