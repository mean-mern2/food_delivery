module.exports = {
    users: require("./user-routes"),
    stores: require("./store-routes"),
    items: require("./menuItems-routes"),
    order:require("./order-routes")
};