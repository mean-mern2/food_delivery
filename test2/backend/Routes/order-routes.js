const express = require("express");
const users = require("../Controller/order-controller");
const router =express.Router();

router.post("/placeorder/:userid",users.placeorder);
router.get("/orderdetails/:id",users.orderDetails);

module.exports=router; 