const express = require("express");
const users = require("../Controller/store-controller");
const router =express.Router();

router.post("/regStore",users.registerStore);
router.get("/getallstores",users.getAllStores);
router.get("/getstore/:id",users.getStore);
router.put("/updateStore/:id",users.updateStore);
router.delete("/deleteStore/:id",users.deleteStore);

module.exports=router; 