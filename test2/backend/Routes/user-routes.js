const express = require("express");
const users = require("../Controller/user-controller");
const router =express.Router();

router.get("/getall",users.getallUser);
router.post("/register",users.register);
router.get("/search/:id",users.getstudent);
router.put("/update/:id",users.updateuser);
router.delete("/delete/:id",users.deleteuser);
router.post("/login",users.loginpage);
router.get("/getprofile",users.getprofile);
router.get("/findallorders/:id",users.findallorders);

module.exports=router; 