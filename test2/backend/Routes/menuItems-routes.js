const express = require("express");
const users = require("../Controller/menuItems-controller");
const router =express.Router();

router.post("/addmenuitem",users.addMenuItem);
router.get("/getallmenuitems",users.getAllMenuItems);
router.get("/getanitem/:id",users.getAnItem);
router.get("/getstoreitems/:id",users.getStoreItems);
router.put("/updateitem/:id",users.updateItem);
router.delete("/deleteitem/:id",users.deleteItem);

module.exports=router; 