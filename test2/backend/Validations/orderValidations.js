const joi = require("joi");


module.exports.joiSchema = joi.object({
   
    userId:joi.string().hex().length(24),
    userName:joi.String(),
    storeId:joi.string().hex().length(24),
    storeName:joi.String(),
    itemId:joi.string().hex().length(24),
    itemName:joi.String(),
    priceofItem: joi.number(),
    quantity:joi.number(),
    totalBill:joi.number()


  });