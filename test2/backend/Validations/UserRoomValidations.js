const joi = require("joi");


module.exports.joiSchema = joi.object({
    user1:joi.string().hex().length(24),
    user2:joi.string().hex().length(24)
  });