const joi = require("joi");

module.exports.joiSchema = joi.object({
    firstname: joi.string().required().min(3).max(10),
    lastname: joi.string().required().min(3),
    email: joi.string().required().email(),
    password: joi.string().required().min(6),
    phoneNo: joi.string().min(6).max(10),
    profilePic: joi.string(),
    isDeactivated:joi.boolean()
  });