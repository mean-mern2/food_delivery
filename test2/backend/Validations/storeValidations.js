
const joi = require("joi");


module.exports.joiSchema = joi.object({
    storeName: joi.string().required(),
    location: joi.string().required()
  });