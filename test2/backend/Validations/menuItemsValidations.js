const joi = require("joi");

module.exports.joiSchema = joi.object({
    storeId: joi.string().hex().length(24).required(),
    storeName:joi.string(),
    itemName: joi.string().required(),
    price: joi.number().required(),
    isAvailable: joi.boolean().required()
  });