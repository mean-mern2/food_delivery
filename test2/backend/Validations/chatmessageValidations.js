const joi = require("joi");


module.exports.joiSchema = joi.object({
    Room_Id:joi.string().hex().length(24),
    Messages: joi.string().required(),
    senderId:joi.string().hex().length(24),
    receiverId:joi.string().hex().length(24)
  });