const express = require('express');
const mongoose = require('mongoose');
const router = require("./Routes/index-routes");
const Model = require("./Model/index");

const http = require("http");
const server = http.createServer(app);
const io = require("socket.io")(server);
require('dotenv').config();
// eslint-disable-next-line no-undef
const PORT = process.env.PORT || 7002;

const {
    createSwagger
} = require("./swagger");
const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const swaggerDocument = YAML.load('./swagger/collection.yml');


const app = express();
app.use(express.json());
app.use("/users", router.users);
app.use("/stores", router.stores);
app.use("/items", router.items);
app.use("/order", router.order);


app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
createSwagger();


io.on("connection", (socket) => {
    socket.emit("welcome", "welcome user");
    socket.on("join-room", async (user1, user2) => {

        const room1 = await Model.UserRoom.UserSchema.findOne({
            "user1": user1,
            "user2": user2
        });
        const room2 = await Model.UserRoom.UserSchema.findOne({
            "user1": user2,
            "user2": user1
        });
        var all_data;

        if (!room1 && !room2) {
            const room = await Model.UserRoom.create(user1, user2);
            socket.join(room._id);
            socket.emit("room-created", room._id);
        } else if (room1) {
            all_data = await Model.ChatMessage.findOne({
                "Room_id": room1._id
            });
            socket.join(room1._id);
            socket.emit("room-joined", room1._id);
            socket.emit(all_data);

        } else if (room2) {
            all_data = await Model.ChatMessage.findOne({
                "Room_id": room2._id
            });
            socket.join(room2._id);
            socket.emit("room-joined", room2._id);
            socket.emit(all_data);
        }


    });
    socket.on("room-message", async (data) => {
        const {
            Room_id,
            Messages,
            senderId,
            receiverId
        } = data;

        const alldata = await Model.UserRoom.UserSchema.findById(Room_id);
        if (!alldata) {
            socket.emit("room not found");
        }


        await Model.ChatMessage.create({
            Room_id,
            Messages,
            senderId,
            receiverId
        });
        socket.to(Room_id).emit('room-message1', {
            Room_id,
            senderId,
            receiverId,
            Messages
        });
    });
    socket.on('disconnect', () => {
        console.log(`User disconnected`);
    });
});

server.listen(PORT, () => {
    mongoose
        .connect("mongodb://127.0.0.1:27017/test2DB")
        .then(() => console.log("DB connected"))
        .catch((err) => console.log(err));
    console.log("connected and listening on port 7002");
});